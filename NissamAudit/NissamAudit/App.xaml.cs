﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using NissamAudit.Model;

namespace NissamAudit
{
	public partial class App : Application
	{
        //the current user of the app
        public static User appUser= null;
        public static NavigationPage Navigation = null;
        public static Label userName = null;
        public static String Address = "http://localhost:8080/SVAudit/mobile";

        public App()
        {
            InitializeComponent();
            Navigation = new NavigationPage(new LoginPage());

            MainPage = Navigation;
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep () 
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}

        public async void OnBackButtonPressed(object sender, EventArgs e)
        {
          
            await Navigation.PopAsync();
        }
    }
}
