﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NissamAudit.Model
{
    public class User
    {
      
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public int Category { get; set; }

        public User(int id, string name, string password, int category)
        {
            this.Id = id;
            this.UserName = name;
            this.Password = password;
            this.Category = category;
        }

    }
}
