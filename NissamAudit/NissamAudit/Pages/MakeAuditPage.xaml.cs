﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace NissamAudit.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MakeAuditPage : ContentPage
	{
		public MakeAuditPage ()
		{
			InitializeComponent ();

            //setting the name of the audit
            if (UserAuditsPage.selectedAudit != null) { 
                AuditName.Text = UserAuditsPage.selectedAudit.Name;
            }


        }
	}
}