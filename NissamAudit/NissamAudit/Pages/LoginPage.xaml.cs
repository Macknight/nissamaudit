﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using NissamAudit.Model;
using NissamAudit.Pages;
using System.Net;
using System.IO;


namespace NissamAudit 
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{

        private  string serverAddress = "http://10.101.102.24:8080/SvAudit/MobileController";
        public static List<Audit> userAudits;
        public static NavigationPage navegation = null;
        private  void ButtonClicked(object sender, EventArgs e)
        {
          //getting values from interface
            string name = userNameEntry.Text;
            string password = passwordEntry.Text;

            User appUser = new User(0, name, password , 1);
            String address  =App.Address;
            
            //sets a static user for the app
            App.appUser = appUser;


         //   getFromServer(serverAddress);

            

       
            //the audits this user is able to do
            userAudits = new List<Audit>();
            //adding sample data
            userAudits.Add(new Audit(1, "LeaderShip Safety Audit- Manufactoring"));
          

            //redirect the navigation control
            Navigation.PushAsync(new ActionsPage());

        }

        private void getFromServer(string serverAddress)
        {
            var request = HttpWebRequest.Create(serverAddress);
            request.ContentType = "text/html";
            request.Method = "GET";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                    Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    var content = reader.ReadToEnd();
                    if (string.IsNullOrWhiteSpace(content))
                    {
                        Console.Out.WriteLine("Response contained empty body...");
                    }
                    else
                    {
                        Console.Out.WriteLine("Response Body: \r\n {0}", content);
                    }

                
                }
            }

        }
        public LoginPage()
        {
            
            InitializeComponent();
           
         
      
            NavigationPage.SetHasNavigationBar(this, false);
        }
   
    }
       
      
}