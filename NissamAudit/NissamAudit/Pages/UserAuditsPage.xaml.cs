﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using NissamAudit.Model;


namespace NissamAudit.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserAuditsPage : ContentPage
	{
		public UserAuditsPage ()
		{
            NavigationPage.SetHasNavigationBar(this, false);
            
            InitializeComponent ();

            Label usernameLabel = Application.Current.FindByName<Label>("UserName");
            if (usernameLabel != null)
            {
                usernameLabel.Text = App.appUser.UserName;
            }
		}


        public static Audit selectedAudit;
        protected override void OnAppearing()
        {
            base.OnAppearing();

            userAuditsListView.ItemsSource = LoginPage.userAudits;

        }

      async void HandleItemSelection(object sendero, SelectedItemChangedEventArgs ei)
        {
            if (ei.SelectedItem == null)
            {
                return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
            }
            Audit selected = (Audit)ei.SelectedItem;
         
         
            //resp receives true if user click yes
            var resp = await this.DisplayAlert("Nova auditoria?", "Gostaria de fazer uma nova "+ selected.Name, "Sim", "Não"); 
            if (resp)
            {
                selectedAudit = selected;
                App.Current.MainPage = new MakeAuditPage();
            }
            else
            {
                //select
                userAuditsListView.SelectedItem = null;
                return;
            }
        
   
            //((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
        }

    }
}